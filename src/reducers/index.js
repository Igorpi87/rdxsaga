import {
    CHANGE_RANGE_FILTER,
    UPDATE_CITY_LIST,
    SELECT_CITY,
    SEARCH_CITY,
    UPDATE_OPTIONS_LIST,
    CLEAR_SELECTED_CITY,
    DELETE_CITY,
    REQUEST_ERROR
} from "../actions";

const initialState = {
    options: [],
    value: "",
    inputValue: "",
    selected: false,
    cityList: [],
    filteredCityList: [],
    rangeValue: 0,
    minRange: 0,
    maxRange: 20
};

export default function cityWeatherApp(state = initialState, action) {
    switch (action.type) {
        case SELECT_CITY:
            return {
                ...state,
                value: action.value,
                selected: true,
                options: []
            };
        case CHANGE_RANGE_FILTER:
            let newFilteredCityList_1 = [...state.cityList].filter(city => city.temp >= action.value);

            return {
                ...state,
                rangeValue: action.value,
                filteredCityList: newFilteredCityList_1
            };
        case CLEAR_SELECTED_CITY:
            return {
                ...state,
                value: "",
                inputValue: "",
                selected: false
            };
        case UPDATE_OPTIONS_LIST:
            return {
                ...state,
                options: action.options
            };
        case REQUEST_ERROR:
            return {
                ...state,
                error: action.error
            };
        case SEARCH_CITY:
            return {
                ...state,
                inputValue: action.value
            };

        case DELETE_CITY:
            let filteredList = [...state.cityList].filter(city => city.id !== action.value);
            let filteredListAfterDelete = [...state.filteredCityList].filter(city => city.id !== action.value);
            return {
                ...state,
                cityList: filteredList,
                filteredCityList: filteredListAfterDelete
            };
        case UPDATE_CITY_LIST:
            let newCityList = [
                ...state.cityList,
                {
                    ...action.data
                }
            ];
            let newFilteredCityList = [...newCityList].filter(city => city.temp >= state.rangeValue);
            return {
                ...state,
                cityList: newCityList,
                filteredCityList: newFilteredCityList,
                value: "",
                inputValue: "",
                selected: false
            };
        default:
            return state;
    }
}
