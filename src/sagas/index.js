import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { REQUEST_ERROR, SEARCH_CITY, UPDATE_OPTIONS_LIST, ADD_CITY_TO_LIST, UPDATE_CITY_LIST } from "../actions";

if (!window.fetch) {
    const fetch = require("whatwg-fetch");
}

function apiCity(val) {
    return fetch(`https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token 3bce5185d9ceeb767783ebcbe3c1c1437767874b"
        },
        body: JSON.stringify({
            query: val,
            count: 4,
            restrict_value: false,
            from_bound: { value: "city" },
            locations: [{ city_type_full: "город" }],
            to_bound: { value: "city" }
        })
    });
}

function apiWhether(val) {
    return fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${encodeURI(val.value.label)}&APPID=41215bab6927e37fb1e810073b64492d&units=metric`
    );
}

export function* fetchCityList(val) {
    try {
        const response = yield call(apiCity, val.value);
        const data = yield response.json();
        let newData = data.suggestions.map(item => ({
            value: item.data.fias_id,
            label: item.data.city
        }));

        yield put({ type: UPDATE_OPTIONS_LIST, options: newData });
    } catch (error) {
        yield put({ type: REQUEST_ERROR, error: error });
    }
}

export function* searchCity() {
    yield takeLatest(SEARCH_CITY, fetchCityList);
}

export function* fetchCityWeather(val) {
    try {
        const response = yield call(apiWhether, val);
        const data = yield response.json();
        if (data.hasOwnProperty("cod") && data.cod === "404") {
            yield put({ type: REQUEST_ERROR, error: data.message });
        } else {
            yield put({
                type: UPDATE_CITY_LIST,
                data: { id: val.value.value, city: val.value.label, data: data, temp: Math.round(Number(data.main.temp)) }
            });
        }
    } catch (error) {
        yield put({ type: REQUEST_ERROR, error: error });
    }
}

export function* searchCityWeather() {
    yield takeLatest(ADD_CITY_TO_LIST, fetchCityWeather);
}

export default function* rootSaga() {
    yield all([searchCity(), searchCityWeather()]);
}
