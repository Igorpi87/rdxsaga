import React from "react";
import InputRange from "react-input-range";

const RangeSlider = ({ rangeValue, minRange, maxRange, changeHandler }) => {
    return (
        <div className="range-slider-container">
            <InputRange
                formatLabel={value => (value === 0 ? "0 °C" : `+${value} °C`)}
                maxValue={maxRange}
                minValue={minRange}
                value={rangeValue}
                onChange={changeHandler}
            />
        </div>
    );
};

export default RangeSlider;
