import React from "react";
import CityItem from "./CityItem";

const CityList = ({ filteredCityList, deleteCity }) => (
    <div className="city-list">
        {filteredCityList.map((cityItem, index) => (
            <CityItem key={index} {...cityItem} deleteCity={deleteCity} />
        ))}
    </div>
);

export default CityList;
