import React from "react";

const CityItem = ({ city, id, data, deleteCity }) => {
    const formatPress = data.hasOwnProperty("main") && data.main.hasOwnProperty("pressure") ? Math.round(data.main.pressure * 0.750062) : "н. д.";
    let temp = "0";
    if (Number(data.main.temp) > 0) temp = `+${Math.round(Number(data.main.temp))}`;
    if (Number(data.main.temp) < 0) temp = `-${Math.round(Number(data.main.temp))}`;
    const wind = Math.round(data.wind.speed);
    const icon = data.weather[0].icon;

    return (
        <div className="city-block">
            <div className="city-title">{city}</div>
            <div className="city-block-delete-btn" onClick={e => deleteCity(id)}></div>
            <div className="city-weather">
                <div className={`city-weather-icon wicon wicon-${icon}`}></div>
                <div className="city-weather-temp">{temp} °C</div>
            </div>
            <div className="city-weather-additional">
                <div className="city-weather-wind">Ветер: {data.wind.speed} м/c</div>
                <div className="city-weather-press">Давление: {formatPress} мм</div>
            </div>
        </div>
    );
};

export default CityItem;
