import { connect } from "react-redux";
import RangeSlider from "../components/RangeSlider";

const mapStateToProps = ({ rangeValue, minRange, maxRange }) => ({ rangeValue, minRange, maxRange });
const mapDispatchToProps = dispatch => ({
    changeHandler: event => dispatch({ type: "CHANGE_RANGE_FILTER", value: event })
});

const RangeSliderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RangeSlider);

export default RangeSliderContainer;
