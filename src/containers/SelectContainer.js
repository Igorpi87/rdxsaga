import { connect } from "react-redux";
import React from "react";

let CitySelect = ({ value, inputValue, options, selected, inputChange, selectChange, clearSelected, addCityToList }) => {
    const inputValueFormatted = selected && value !== null ? value.label : inputValue;
    const border = options.length > 0 ? "1px solid #ccc" : "none";

    return (
        <div className="city-select-container">
            <div className="city-select-form">
                <input type="text" className="city-select-input" readOnly={selected} value={inputValueFormatted} onChange={inputChange} />
                <button className="city-select-button" disabled={!selected} onClick={e => addCityToList(value)}>
                    <span className="city-select-button-add-icon"></span>
                </button>
            </div>
            {selected && <div className="city-select-button-clear-icon" onClick={clearSelected} />}
            <ul className="city-select-suggest-list" onClick={selectChange} style={{ border: border }}>
                {options.length > 0 &&
                    options.map(item => (
                        <li data-item={JSON.stringify(item)} className="city-select-suggest-item" key={item.value}>
                            {item.label}
                        </li>
                    ))}
            </ul>
        </div>
    );
};

const mapStateToProps = ({ value, options, inputValue, selected }) => ({
    value,
    options,
    inputValue,
    selected
});

const mapDispatchToProps = dispatch => ({
    selectChange: event => {
        const data = JSON.parse(event.target.getAttribute("data-item"));
        if (data !== null)
            dispatch({
                type: "SELECT_CITY",
                value: data
            });
    },
    inputChange: event => dispatch({ type: "SEARCH_CITY", value: event.target.value }),
    clearSelected: () => dispatch({ type: "CLEAR_SELECTED_CITY" }),
    addCityToList: val => dispatch({ type: "ADD_CITY_TO_LIST", value: val })
});

const SelectContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CitySelect);

export default SelectContainer;
