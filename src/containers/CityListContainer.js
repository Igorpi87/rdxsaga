import { connect } from "react-redux";
import CityList from "../components/CityList";

const mapStateToProps = ({ filteredCityList }) => ({ filteredCityList });
const mapDispatchToProps = dispatch => ({
    deleteCity: id => dispatch({ type: "DELETE_CITY", value: id })
});

const CityListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CityList);

export default CityListContainer;
