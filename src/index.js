import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { logger } from "redux-logger";
import createSagaMiddleware from "redux-saga";

import App from "./App";
import cityWeatherApp from "./reducers";
import rootSaga from "./sagas";

import "./styles/index.css";
import "bootstrap-css-only";
import "react-input-range/lib/css/index.css";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  cityWeatherApp,
  applyMiddleware(sagaMiddleware, logger)
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
