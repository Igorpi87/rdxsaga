import React from "react";
import SelectContainer from "./containers/SelectContainer";
import CityListContainer from "./containers/CityListContainer";
import RangeSliderContainer from "./containers/RangeSliderContainer";

function App() {
    return (
        <div className="App">
            <div className="container main-content">
                <div className="main-controls">
                    <SelectContainer />
                    <RangeSliderContainer />
                </div>
                <CityListContainer />
            </div>
        </div>
    );
}

export default App;
